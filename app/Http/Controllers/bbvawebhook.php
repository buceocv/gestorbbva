<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Anotacion;
use App\Cuenta;

use App\Clases\Depositos;
use App\Clases\Retiradas;
use App\Clases\Nominas;
use App\Clases\TransRecibida;
use App\Clases\TransEnviada;

use Illuminate\Http\Request;
use Log;
class bbvawebhook extends Controller
{
    //Webhook del endpoint para recepcion de avisos APIbbva
    public function notifi(Request $request) {

        //decodifica la respuesta de la web en array , selecciona la parte contenido y la almacena en variable
        $jsondec = json_decode($request->getContent() , true);
        
        for($i=0;$i<count($jsondec);$i++){ //rastrea todos los mensajes                   
            
 

            // Carga de tipo en base al evento recibido 
            switch ($jsondec[0]['eventType']) {
            
                case 'ACCOUNT.DEPOSIT': //ingreso generico en cuenta OK
                     
                 $deposito = new Depositos();

                    $deposito->creadeposito($jsondec[$i]['payload']['valueDate'],
                                            $jsondec[$i]['payload']['description'],
                                            $jsondec[$i]['payload']['receiver']['account']['format']['iban'],
                                            'deposito' ,
                                            $jsondec[$i]['payload']['amount']['amount'] );
                 
                 break;
                
                case 'ACCOUNT.WITHDRAWAL': // retirada de cuenta OK

                    $retirada = new Retiradas();

                    $retirada->crearetirada($jsondec[$i]['payload']['valueDate'],
                                            $jsondec[$i]['payload']['description'],
                                            $jsondec[$i]['payload']['receiver']['account']['format']['iban'],
                                            'retirada' ,
                                            $jsondec[$i]['payload']['amount']['amount'] );
                    

                break;

                case 'TRANSFER.RECEPTION.PAYROLL': // Ingreso de nomina OK
                $nomina = new Nominas();

                $nomina->creanomina($jsondec[$i]['payload']['valueDate'],
                                    $jsondec[$i]['payload']['transactionType']['literal'],
                                    $jsondec[$i]['payload']['receiver']['account']['format']['iban'],
                                    'nomina' ,
                                    $jsondec[$i]['payload']['amount']['amount'] );

                break;

                case 'TRANSFER.NATIONAL.RECEPTION': // TRANSFERENCIA Nacional RECIBIDA
                $TransRecibida = new TransRecibida();

                $TransRecibida->creatransrecibida($jsondec[$i]['payload']['valueDate'],
                                                  $jsondec[$i]['payload']['concept'],
                                                  $jsondec[$i]['payload']['receiver']['account']['format']['iban'],
                                                  'TransRecibida' ,
                                                  $jsondec[$i]['payload']['amount']['amount'] );

                break;

                case 'TRANSFER.NATIONAL.SENDING': // TRANSFERENCIA Nacional enviada

                $TransEnviada = new TransEnviada();

                $TransEnviada->creatransenviada($jsondec[$i]['payload']['valueDate'],
                                                  $jsondec[$i]['payload']['concept'],
                                                  $jsondec[$i]['payload']['receiver']['account']['format']['iban'],
                                                  'TransEnviada' ,
                                                  $jsondec[$i]['payload']['amount']['amount'] );

                break;

                /*IMPLERMENTAR FUNCIONES DE TARJETAS PARA LA SIGUIENTE VERSION */ 
                

                default:
                    $tipo = 'desconocido';
                    $descripcion = 'DESCONOCIDO';
                    $fecha = '2012-12-12';
                    $signo = '+';
                    $iban = 'ES000000000000';
                    $valor =0;
         
                break;


            } // end swich
            Log::info($jsondec[$i]);
        } //end For
            
    } // end Function

} //end class

                  