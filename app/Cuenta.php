<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    //
    public $table = "cuentas";
    public $timestamps = false;
}
