<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearCuentasTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('cuentas', function (Blueprint $table) 
       {

           $table->string('iban' , 30)->primary();
           $table->string('alias' , 40);
           $table->float('cantidad' , 20 ,2);


       });
   }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      

    }
}
