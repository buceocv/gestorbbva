FROM php:7.3.3-apache-stretch

RUN apt-get update

RUN apt-get install -y \
	curl \
	git \
	supervisor \
	zip \
	unzip

RUN apt-get install -y libpq-dev 
RUN docker-php-ext-install pdo pdo_mysql

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN a2enmod rewrite

ENV APP_HOME /var/www/html

RUN mkdir -p /opt/data/public && \
	rm -r /var/www/html && \
   ln -s /opt/data/public $APP_HOME

RUN cd /opt/data && \ 
      composer install 

WORKDIR $APP_HOME
