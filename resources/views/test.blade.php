@extends('plantillas.plantilla')
@section ('cabecera')

@endsection

    <body>
<div class="izquierda">
        <table class="tablaCuentas">
            <caption>Estado de las cuentas</caption>
             
            <tr>
                <th>ID</th> <th>IBAN</th> <th>Nombre de la cuenta</th> <th>Cantidad</th>
            </tr>

            <tr>
               <td>IDdato</td> <td>IBANdato</td> <td>Nombre de la cuentadato</td> <td>Cantidaddato</td>
            </tr>
        </table>
</div>
<div class="derecha">
        <table class="tablaResumenGastos">
            <caption>MES ACTUAL</caption>
            <tr>
                <th> Gastos fijos Mes en curso: </th>  <th> Valor de </th>                            
            </tr>
            <tr>
                    <th> Gastos fijos YA PAGADOS: </th>  <th> Valor de </th>                            
            </tr>

            <tr>
                <th> Gastos Totales este mes: </th>  <th> Valor de </th>                            
            </tr>
            <tr>
                    <th> Gastos Totales YA PAGADOS: </th>  <th> Valor de </th>                            
            </tr>

            <tr>
                    <th> Balance de cuenta principal: </th>  <th> Valor de </th>                            
                </tr>
        </table>

        <table class="tablaResumenOtros">
                <caption>OTROS MESES</caption>
                <tr>
                    <th> Gastos fijos Mes SIGUIENTE: </th>  <th> Valor de </th>                            
                </tr>
                <tr>
                    <th> TOTAL DE GASTOS MES ANTERIOR: </th>  <th> Valor de </th>                            
                </tr>
    
                <tr>
                    <th> TOTAL AHORRADO MES ANTERIOR: </th>  <th> Valor de </th>                            
                </tr>

            </table>
</div>

    </body>

</html>
