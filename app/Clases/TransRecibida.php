<?php

namespace app\Clases;

use App\Anotacion;
use App\Cuenta;
use Illuminate\Support\Facades\DB;
use Log;
    class TransRecibida 
    {
        function creatransrecibida($fecha , $descripcion , $iban , $tipo , $valor)
        {
            //Nueva anotacion en tabla 
            $nuevanota = new Anotacion(); 
                
            $nuevanota -> fecha = $fecha;
            $nuevanota -> descripcion = $descripcion;
            $nuevanota -> iban = $iban;
            $nuevanota -> tipo = $tipo;
            $nuevanota -> cantidad = $valor;
            $nuevanota -> categoria_id = 1;

            $nuevanota->save(); //graba los datos en ddbb ANOTACIONES

            //Actualizacion de valor de CUENTA
             //consulta da como resultado el array del registro 
            $registroCuenta = DB::selectone('SELECT cantidad FROM cuentas WHERE iban = ?' , [$iban]);       
             // recogemos el valor del array del registro (celda)
            $valorAnt= $registroCuenta-> cantidad ; 
             
            $valorActualizar = $valorAnt + $valor ; 
            Log::info($valorAnt );
            Log::info($valor );
             
             DB::update('UPDATE cuentas SET cantidad = ? WHERE iban= ?' , [$valorActualizar , $iban]);
     }

    }



?>