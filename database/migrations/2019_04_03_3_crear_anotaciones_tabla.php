<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearAnotacionesTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
        Schema::create('anotaciones', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->date('fecha');
            $table->string('descripcion' , 50);
            $table->string('iban' , 30)->index();;
            $table->string('tipo' , 40);
            $table->float('cantidad' , 20 ,2);
            $table->unsignedInteger('categoria_id')->index();
            $table->foreign('categoria_id')
                            ->references('id')
                                        ->on('categorias')->onDelete('cascade');
            $table->foreign('iban')
                            ->references('iban')
                                        ->on('cuentas')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
