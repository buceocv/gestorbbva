<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anotacion extends Model
{
    //
    public $table = "anotaciones";
    public $timestamps = false;
}
