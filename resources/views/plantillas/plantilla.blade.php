<!@php <!DOCTYPE html>
    <html lang=es>

    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="{{ asset('css/estilos.css') }}" />



        <title>plantilla de blade</title>

    </head>

    <body>



        <div class="cabecera">
            @yield("cabecera")


        </div>



        <div class="cuerpo">
            @yield('cuerpo')



        </div>

        <div class="pie">
            @yield('pie')

        </div>


    </body>

    </html>